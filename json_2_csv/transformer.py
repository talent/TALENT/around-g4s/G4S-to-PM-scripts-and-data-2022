import json
import sys
import argparse
from collections import defaultdict
from json import JSONDecodeError
from pathlib import Path
from csv import writer

from faker import Factory, Faker

parser = argparse.ArgumentParser(description="A tool to transform Git4School JSON logs into CSV",
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("-d", "--dst", type=str, help="the folder to save the csv into")
parser.add_argument("-r", "--rename", type=str, help="rename the CSV file")
parser.add_argument("src", type=str, help="the git4school log json file")
parser.add_argument("--anonymize", default=False, action="store_true",
                    help="this option anonymize the data with fake name")

args = parser.parse_args()

try:
    src_filename = args.src
    src_path = Path(src_filename).resolve(strict=True)

    dst_folder = Path(args.dst or str(src_path.parent)).resolve(strict=True)
    dst_filename = Path(args.rename or str(src_path.stem)).with_suffix(".csv")

    dst_path = dst_folder / dst_filename

    anonymize = args.anonymize 

    headers = ['author', 'commitAuthor', 'tpGroup', 'message', 'color', 'question', 'commitDate']
    rows = []

    faker = Faker("fr_FR")
    names = defaultdict(faker.unique.name)

    with open(src_path, 'r', encoding='UTF-8') as f:
        repositories = json.load(f)['repositories']
        for repo in repositories:
            for commit in [a for a in repo['commits']]:  # if "question" in a and a['isCloture']]:
                row = [names[repo['name']] if anonymize else repo['name'],
                       names[commit['author']] if commit['author'] != "github-classroom[bot]" and
                       anonymize else commit['author'],
                       repo['tpGroup'],
                       commit['message'],
                       commit['color']['label'], commit['question'] if 'question' in commit else
                       "", commit['commitDate']]
                rows.append(row)

    with open(dst_path, 'w', encoding='UTF-8', newline='') as csv:
        writer = writer(csv)
        writer.writerow(headers)
        writer.writerows(rows)
        print(f"The file {dst_path.name} has been created!")

except FileNotFoundError as file_nf_err:
    print("File not found error : ", str(file_nf_err))
except JSONDecodeError as json_decode_err:
    print("Reading JSON error : ", str(json_decode_err))
except IOError as IO_err:
    print("Writing error : ", IO_err)
