# Finding behavioral indicators from contextualized commits in software engineering courses with process mining

This public repository makes accessible commits logs as datasets, reports of analysis with Process Mining, as well as the scripts that produced them.
The root of the repository is constituted of the following folders:

- `data/` which contains the anonymized datasets ;
- `graphs/` which contains the 2 reports including the visualizations obtained with the analysis;
- `json_2_CSV/` which contains the script "transform.py" allowing to convert the G4S exports into CSV files while anonymizing the data;
- `pm_script/` which contains the R markdown scripts generating the graphs seen above.

## G4S exports transformer (`json_2_CSV/`)

### How to install the dependencies

Before doing any operation, you need to have these requirements installed :

- _python 3.7_
- _pipenv_ installed in the python above

To manage the dependencies, we use the tool [Pipenv](https://pipenv.pypa.io/en/latest/). It also
creates a virtual environment called **venv**.

With that done, you can install the dependencies with the following command :

`pipenv install --dev`

### How to use the script

To use the script, you can use the following command :

`pipenv run python transformer.py <log_file.json>`

> The script has several options to save the output file in another folder or under another name.
> You can look at them with the following command : `pipenv run python transformer.py --help`

## R markdown reports scripts (`pm_script/`)

To get the script working, you have to set your working directory path _line 29_, which should be the `data/` directory.

The 6 following lines should not be changed unless you want to analyze other files or rename the existing ones.
